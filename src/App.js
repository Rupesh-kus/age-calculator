import React, { Component } from "react";

import AgeStats from "./Component/AgeStats";
import { Form, FormControl, Button } from "react-bootstrap";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      newDate: "",
      birthday: "",
      showStats: false,
    };
  }

  changeBirthday() {
    console.log(this.state);
    this.setState({
      birthday: this.state.newDate,
      showStats: true,
    });
  }

  render() {
    return (
      <div className='App'>
        <Form inline>
          <h2> Input your Birthday !</h2>
          <FormControl
            type='date'
            onChange={(event) => this.setState({ newDate: event.target.value })}
          ></FormControl>
          <Button onClick={() => this.changeBirthday()}> submit</Button>
          {this.state.showStats ? (
            <div className='fade age-stats'>
              <AgeStats date={this.state.birthday} />
            </div>
          ) : (
            <div></div>
          )}
        </Form>
      </div>
    );
  }
}

export default App;
